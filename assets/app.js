/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';

const useSortableData = (items, config = null) => {
    const [sortConfig, setSortConfig] = React.useState(config);

    const sortedItems = React.useMemo(() => {
        let sortableItems = [...items];
        if (sortConfig !== null) {
            sortableItems.sort((a, b) => {
                if (a[sortConfig.key] < b[sortConfig.key]) {
                    return sortConfig.direction === 'ascending' ? -1 : 1;
                }
                if (a[sortConfig.key] > b[sortConfig.key]) {
                    return sortConfig.direction === 'ascending' ? 1 : -1;
                }
                return 0;
            });
        }
        return sortableItems;
    }, [items, sortConfig]);

    const requestSort = (key) => {
        let direction = 'ascending';
        if (
            sortConfig &&
            sortConfig.key === key &&
            sortConfig.direction === 'ascending'
        ) {
            direction = 'descending';
        }
        setSortConfig({key, direction});
    };

    return {items: sortedItems, requestSort, sortConfig};
};

function getUnique(items, key) {
    const arrayUniqueByKey = [...new Map(items.map(item =>
        [item[key], item])).values()];

    return arrayUniqueByKey;
}

const SongTable = (props) => {
    const {items, requestSort, sortConfig} = useSortableData(props.songs);
    const getClassNamesFor = (name) => {
        if (!sortConfig) {
            return;
        }
        return sortConfig.key === name ? sortConfig.direction : undefined;
    };
    return (
        <table>
            <caption>Songs</caption>
            <thead>
            <tr>
                <th>
                    <button
                        type="button"
                        onClick={() => requestSort('artist')}
                        className={getClassNamesFor('artist')}
                    >
                        Artist
                    </button>
                </th>
                <th>
                    <button
                        type="button"
                        onClick={() => requestSort('name')}
                        className={getClassNamesFor('name')}
                    >
                        Name
                    </button>
                </th>
                <th>
                    <button
                        type="button"
                        onClick={() => requestSort('genre')}
                        className={getClassNamesFor('genre')}
                    >
                        Genre
                    </button>
                </th>
                <th>
                    <button
                        type="button"
                        onClick={() => requestSort('year')}
                        className={getClassNamesFor('year')}
                    >
                        Year
                    </button>
                </th>
            </tr>
            </thead>
            <tbody>
            {items.map((item) => (
                <tr key={item.id}>
                    <td>{item.artist}</td>
                    <td>{item.name}</td>
                    <td>{item.genre}</td>
                    <td>{item.year}</td>
                </tr>
            ))}
            </tbody>
            <tfoot>
            <label>
                Artist
                <select onChange={}>
                    {
                        getUnique(items, 'artist').map((item) => (
                            <option key={item.id}>{item.artist}</option>
                        ))
                    }
                </select>
            </label>
            <label>
                Genre
                <select onChange={}>
                    {
                        getUnique(items, 'genre').map((item) => (
                            <option key={item.id}>{item.genre}</option>
                        ))
                    }
                </select>
            </label>
            <label>
                Year
                <select onChange={}>
                    {
                        getUnique(items, 'year').map((item) => (
                            <option key={item.id}>{item.year}</option>
                        ))
                    }
                </select>
            </label>
            </tfoot>
        </table>
    );
};

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        }
    }

    componentDidMount() {
        fetch('http://localhost:8000/songs')
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },

                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        console.log(this.state);
        const {error, isLoaded, items} = this.state;

        if (error) {
            return <div>ERROR</div>
        } else if (!isLoaded) {
            return <div>Loading</div>
        } else {
            return (<SongTable songs={items}>

            </SongTable>)
        }
    }
}

ReactDOM.render(<Table/>, document.getElementById('root'));
