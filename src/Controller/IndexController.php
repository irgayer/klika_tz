<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    #[Route('/songs', name: 'songs')]
    public function songs() : JsonResponse
    {
        return new JsonResponse([
            ['id' => 1,'artist' => 'Blink-182', 'name' => 'First Date', 'year' => 2001, 'genre' => 'pop punk'],
            ['id' => 2,'artist' => 'Blink-182', 'name' => 'All The Small Things', 'year' => 2000, 'genre' => 'pop punk'],
            ['id' => 3,'artist' => 'Blink-182', 'name' => 'I Miss You', 'year' => 2004, 'genre' => 'pop punk'],
            ['id' => 5,'artist' => 'Bob Dylan', 'name' => 'Like a Rolling Stone', 'year' => 1965, 'genre' => 'country'],
            ['id' => 6,'artist' => 'The Rolling Stones', 'name' => 'Satisfaction', 'year' => 1965, 'genre' => 'blues-rock'],
            ['id' => 7,'artist' => 'John Lennon', 'name' => 'Imagine', 'year' => 1971, 'genre' => 'ballade'],
            ['id' => 8,'artist' => 'Marvin Pentz Gaye', 'name' => 'What’s Going On', 'year' => 1971, 'genre' => 'soul'],
            ['id' => 9,'artist' => 'Aretha Louise Franklin', 'name' => 'Respect', 'year' => 1967, 'genre' => 'soul'],
            ['id' => 10,'artist' => 'The Beatles', 'name' => 'Hey Jude', 'year' => 1968, 'genre' => 'rock'],
            ['id' => 11,'artist' => 'Nirvana', 'name' => 'Smells Like Teen Spirit', 'year' => 1991, 'genre' => 'rock'],
            ['id' => 12,'artist' => 'The Who', 'name' => 'My Generation', 'year' => 1965, 'genre' => 'rock'],
            ['id' => 13,'artist' => 'The Beatles', 'name' => 'Yesterday', 'year' => 1965, 'genre' => 'ballade'],
            ['id' => 14,'artist' => 'The Beatles', 'name' => 'I Want to Hold Your Hand', 'year' => 1963, 'genre' => 'pop'],
            ['id' => 15,'artist' => 'The Beatles', 'name' => 'Let It Be', 'year' => 1970, 'genre' => 'ballade'],
            ['id' => 16,'artist' => 'The Ronettes', 'name' => 'Be My Baby', 'year' => 1963, 'genre' => 'pop'],
            ['id' => 17,'artist' => 'U2', 'name' => 'Moment of Surrender', 'year' => 2009, 'genre' => 'rock'],
            ['id' => 18,'artist' => 'Green Day', 'name' => 'American Idiot', 'year' => 2004, 'genre' => 'pop punk'],
            ['id' => 19,'artist' => 'Daft Punk', 'name' => 'One More Time', 'year' => 2000, 'genre' => 'house'],
            ['id' => 20,'artist' => 'U2', 'name' => 'Beautiful Day', 'year' => 2000, 'genre' => 'rock'],
            ['id' => 21,'artist' => 'Radiohead', 'name' => 'Fake Plastic Trees', 'year' => 1995, 'genre' => 'rock'],
            ['id' => 22, 'artist' => 'The Notorious B.I.G.', 'name' => 'Juicy', 'year' => 1994, 'genre' => 'rap'],
            ['id' => 23,'artist' => 'Jay-Z', 'name' => 'Big Pimpin’', 'year' => 1999, 'genre' => 'rap'],
        ]);
    }
}
